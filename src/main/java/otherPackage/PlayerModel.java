package otherPackage;

public class PlayerModel {

	private String playerName;
	private String random_code;
	
	public String getPlayerName() {
		return playerName;
	}
	
	public void setPerson(String playerName) {
		this.playerName = playerName;
	}
	
	public String getRandom_code() {
		return random_code;
	}
	
	public void setRandom_code(String random_code) {
		this.random_code = random_code;
	}
}
