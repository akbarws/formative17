package otherPackage;

public class PlayerView {
	
	public void printWinner(PlayerController controller, PlayerModel player) {
		System.out.println("Winner Code : "+player.getRandom_code());
		System.out.println("Winner Name : "+player.getPlayerName());
		System.out.println("Winner Prize : "+controller.getListPlayer().size() * controller.getPrize());
		System.out.println("##############################");
	}
	
	public void printNewLine(String[] messages) {
		for(int i=0; i<messages.length; i++) {
			System.out.println(messages[i]);
		}
	}
	
}
