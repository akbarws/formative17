package otherPackage;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;
import java.util.logging.Logger;
import java.util.regex.Pattern;

public class PlayerController {
	
	private ArrayList<PlayerModel> listPlayer = new ArrayList<PlayerModel>();
	private ArrayList<PlayerModel> listWinner = new ArrayList<PlayerModel>();
	
	private PlayerView playerView = new PlayerView();
	
	private Scanner scan = new Scanner(System.in);
	private Logger log = Logger.getLogger("PlayerController.class");
	
	private int prize = 150000;

	protected int getPrize() {
		return prize;
	}

	public ArrayList<PlayerModel> getListPlayer() {
		return listPlayer;
	}
	
	private void addPlayer(String personName) {
		PlayerModel newPlayer = new PlayerModel();
		newPlayer.setPerson(personName);
		this.getListPlayer().add(newPlayer);
	}

	private ArrayList<PlayerModel> getListWinner() {
		return listWinner;
	}

	protected PlayerModel getWinner(ArrayList<PlayerModel> listPlayer, ArrayList<PlayerModel> listWinner) {
		System.out.println("Arisan started ...");
		this.setRandomCode();
		int indexWinner = new Random().nextInt(listPlayer.size());
		PlayerModel playerWinner = listPlayer.get(indexWinner); 
		listPlayer.remove(indexWinner);
		listWinner.add(playerWinner);
		return playerWinner;
	}

	private void setRandomCode() {
		this.getListPlayer().forEach(player -> {
		    player.setRandom_code(this.getRandomCode());
		});
		System.out.println("Random Code Generated ...");
	}
	
	protected String getRandomCode() {
		int leftLimit = 97;
	    int rightLimit = 122;
	    int targetStringLength = 5;
	    Random random = new Random();
	    StringBuilder buffer = new StringBuilder(targetStringLength);
	    for (int i = 0; i < targetStringLength; i++) {
	        int randomLimitedInt = leftLimit + (int) 
	          (random.nextFloat() * (rightLimit - leftLimit + 1));
	        buffer.append((char) randomLimitedInt);
	    }
	    return buffer.toString();
	}
	
	private void addPerson() {
		boolean isValid = false;
		do {
			System.out.print("Input name : ");
			String personName = scan.nextLine();
			String regexName = "^[a-zA-Z]+(?:[\\s.]?[a-zA-Z]+)*$";
			if(Pattern.matches(regexName, personName)) {
				isValid = true;
				this.addPlayer(personName);;
				log.info("Person added "+personName);
			} else {
				System.out.println("Not Valid");
			}
		} while(!isValid);
	}
	
	protected String inputMenu() {
		playerView.printNewLine(new String[] {"1. Add person","2. Get Winner","3. Exit"});
		System.out.print("Masukkan pilihan : ");
		String userChoice = scan.nextLine();
		System.out.println("##############################");
		return userChoice;
	}
	
	protected boolean inputMenuValidation (String userChoice) {
		String regexMenu = "^[1-3]";
		return Pattern.matches(regexMenu, userChoice);
	}
	
	public void play() {
		boolean isExit = false;
		do {
			String userChoice = inputMenu();
			if(inputMenuValidation(userChoice)) {
				switch(userChoice) {
					case "1" :
						this.addPerson();
						break;
					case "2" :
						if(this.getListPlayer().size()==0) {
							if(this.getListWinner().size()==0) {
								System.out.println("Nobody ...");
							} else {								
								playerView.printNewLine(new String[] {
										"Everyone is winner","Application exit ..."});
								isExit = true;
							}
						} else {
							PlayerModel player = this.getWinner(this.getListPlayer(), this.getListWinner());
							playerView.printWinner(this, player);
						}
						break;
					default : isExit = true;
				}
			} else {
				System.out.println("Not Valid");
			}
		} while(!isExit);
	}
}
