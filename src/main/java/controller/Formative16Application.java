package controller;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import otherPackage.PlayerController;

@SpringBootApplication
public class Formative16Application {
	
	public static void main(String[] args) {
		SpringApplication.run(Formative16Application.class, args);
		PlayerController play = new PlayerController();
		play.play();
	}

}
