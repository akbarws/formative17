package otherPackage;

import static org.junit.jupiter.api.Assertions.*;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.RepetitionInfo;
import org.junit.jupiter.api.Test;

class PlayerControllerTest {
	
	PlayerController pc;
	PlayerView pv;
	
	@BeforeEach
	void before() {
		pc = new PlayerController();
	}

	@Test
	void testGetRandomCode() {
		String randomCode = pc.getRandomCode();
		assertEquals(5, randomCode.length());
	}
	
	@RepeatedTest(value=3, name="{currentRepetition}")
	void testInputMenuValidation(RepetitionInfo repeatitionInfo) {
		String menuChoice[] = {"1", "2", "3"};
		boolean inputMenu = pc.inputMenuValidation(menuChoice[repeatitionInfo.getCurrentRepetition()-1]);
		assertTrue(inputMenu);
	}
	
	@Test
	void testGetWinner() {
		ArrayList<PlayerModel> listPlayer = new ArrayList<PlayerModel>();
		PlayerModel newPlayer = new PlayerModel();
		String nameWinner = "Akbar";
		newPlayer.setPerson(nameWinner);
		listPlayer.add(newPlayer);
		ArrayList<PlayerModel> listWinner = new ArrayList<PlayerModel>();
		PlayerModel playerWinnerResult = pc.getWinner(listPlayer, listWinner);
		assertEquals(nameWinner, playerWinnerResult.getPlayerName());
	}

	@Test
	void testInputMenu() {
		String inputUserMenu = pc.inputMenu();
		String input = "1";
		InputStream in = new ByteArrayInputStream(input.getBytes());
		System.setIn(in);
		assertEquals(input, inputUserMenu);
	}
	
	@Test
	void testPrintWinner() {
		
	}

}
